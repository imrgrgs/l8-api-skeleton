<?php
return [
    /*
    |----------------------------------------------------------------------
    | locales params
    |----------------------------------------------------------------------
    */
    'locales' => [
        'pt-BR' => [
            'value' => 'pt-BR',
            'text' => 'Português-Brasil',
            'is_visible' => true,
            'is_default' => true,
            'is_active' => true,
        ],
        'en' => [
            'value' => 'en',
            'text' => 'Inglês-USA',
            'is_visible' => true,
            'is_default' => false,
            'is_active' => true,
        ],
        'es' => [
            'value' => 'es',
            'text' => 'Espanhol',
            'is_visible' => true,
            'is_default' => false,
            'is_active' => true,
        ],
        'it' => [
            'value' => 'it',
            'text' => 'Italiano',
            'is_visible' => true,
            'is_default' => false,
            'is_active' => true,
        ],
    ],

    /*
    |----------------------------------------------------------------------
    | user groups params
    |----------------------------------------------------------------------
    */
    'group_access' => [
        'backend' => [
            'value' => 'backend',
            'text' => 'grupo de controle de retagaurda do sistema',
            'is_visible' => true,
            'is_default' => true,
            'is_active' => true,
        ],
        'frontend' => [
            'value' => 'frontend',
            'text' => 'grupo de usuários do sistema',
            'is_visible' => true,
            'is_default' => false,
            'is_active' => true,
        ],
    ],
];
