<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//////////////START V1 AUTH//////////////////////////////////////////////
$version = 'V1';
$groupTo = '';
$namespace = 'App\\Http\\Controllers\\API\\' . $version;
$group = [
    'prefix' => strtolower($version),
    'domain' => '',
    'middleware' => ['localization'],
    'as' => strtolower($version) . '.',
    'namespace' => $namespace,
];

Route::group($group, function () {
    Route::post(
        'login',
        [
            'as' => 'login',
            'uses' => 'Auth\AuthController@login',
        ]
    );

    Route::group(['middleware' => ['jwt.auth']], function () {
        Route::post(
            'logout',
            [
                'as' => 'logout',
                'uses' => 'Auth\AuthController@logout',
            ]
        );
        Route::post(
            'refresh',
            [
                'as' => 'refresh',
                'uses' => 'Auth\AuthController@refresh',
            ]
        );
    });
});
//////////////END V1 AUTH//////////////////////////////////////////////




//////////////START V1 BACKEND//////////////////////////////////////////////
$version = 'V1';
$groupTo = 'Backend';
$namespace = 'App\\Http\\Controllers\\API\\' . $version . '\\' . $groupTo;
$group = [
    'prefix' => strtolower($version)  . '/' . strtolower($groupTo),
    'domain' => '',
    'middleware' => ['localization'],
    'as' => strtolower($version) . '.' . strtolower($groupTo) . '.',
    'namespace' => $namespace,
];

Route::group($group, function () {
    Route::group(['middleware' => ['jwt.auth']], function () {
        Route::post(
            'users',
            [
                'as' => 'users.store',
                'uses' => 'User\UserController@store',
            ]
        );
    });
});
//////////////END V1 BACKEND//////////////////////////////////////////////



//////////////START V1 FRONTEND//////////////////////////////////////////////
$version = 'V1';
$groupTo = 'Frontend';
$namespace = 'App\\Http\\Controllers\\API\\' . $version . '\\' . $groupTo;
$group = [
    'prefix' => strtolower($version)  . '/' . strtolower($groupTo),
    'domain' => '',
    'middleware' => ['localization'],
    'as' => strtolower($version)  . '.' . strtolower($groupTo) . '.',
    'namespace' => $namespace,
];

Route::group($group, function () {
});
//////////////END V1 FRONTEND//////////////////////////////////////////////


Route::fallback(function () {
    return response()->json([
        'status' => 404,
        'success' => false,
        'message' => 'api-manager:Page Not Found. If error persists, contact ti@handsonmanager.com.br',
        'data' => null
    ], 404);
});
