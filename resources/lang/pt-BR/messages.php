<?php

return [
    'locale_not_allowed' => 'localidade :attribute não permitida. Localidades permitidas são: :allowed',
    'deleted' => ':model registro foi excluído com sucesso!',
    'saved' => ':model registro foi salvo com sucesso!',
    'retrieved' => ':model registro foi obtido com sucesso!',
    'updated' => ':model registro foi alterado com sucesso!',
    'entry_not_found' => ':model registro não encontrado.',
    'logout' => 'logout efetuado com sucesso.',
    'refresh_token' => 'refresh token was successfully.',
    'is_thumb_not_delete' => ':model é miniatura',
    'not_same_enterprising' => ':model não pertence ao empreendedor do seu usuário',
    'not_has_enterprising' => ':model não pode ser atualizado por usuário sem empreendedor',
    'http_internal_server_error' => 'Ops, estamos com problemas no servidor. Mas nossos técnicos já estão trabalhando para resolver isso. Tente novamente dentro de alguns minutos, por gentileza.',
    'not_authorized_app' => 'Você não tem permissão para utilizar o APP',
    'not_authorized_list' => 'Você não tem permissão para listar :model',
    'not_authorized_create' => 'Você não tem permissão para adicionar :model',
    'not_authorized_read' => 'Você não tem permissão para visualizar :model',
    'not_authorized_update' => 'Você não tem permissão para alterar :model',
    'not_authorized_delete' => 'Você não tem permissão para excluir :model',
    'not_authorized_import' => 'Você não tem permissão para importar :model',
    'not_authorized_export' => 'Você não tem permissão para exportar :model',
    'not_authorized_print' => 'Você não tem permissão para imprimir :model',
    'not_authorized_login' => 'Você não tem permissão de acesso no momento',
    'request_has_errors' => 'A solicitação possuí erros',
    'entity_has_dependency' => ':model operação não pode ser executada existe dependência em outras tabelas',
    'not_authorized_delete_node' => 'Não é permitido excluir o nó principal :model',
    'not_allowed_measurements_not_is_leaf' => 'Não é permitido lançar medições para níveis que não são atividade na :model',

    'not_delete_doing_done' => ':model Não é permitido excluir atividade em execuçao ou encerrada ',
    'descendants_not_delete_doing_done' => ':model Não é possível excluir, existem atividades filhas em execuçao ou encerradas ',

    'not_authorized_delete_node' => 'Não é permitido excluir o nó principal :model',

    'email_send_success' => ':model envio de e-mail com sucesso',

    'same_user' => 'Você não pode :action a si mesmo',


    'app' => [
        'register' => 'Verifique em seu e-mail as instruções para completar o seu registro e termos de uso do aplicativo',
        'forget_password' => 'Verifique em seu e-mail as instruções para recuperação de sua senha',
        'forget_password_changed' => 'Sua senha foi alterada com sucesso!',
    ]

];
