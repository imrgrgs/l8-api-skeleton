<?php

return [
    'locale_not_allowed' => 'locale :attribute not allowed. Allowed locales are: :allowed',

    'retrieved' => ':model retrieved successfully.',
    'saved' => ':model saved successfully.',
    'updated' => ':model updated successfully.',
    'deleted' => ':model deleted successfully.',
    'entry_not_found' => ':model not found',
    'logout' => 'logout was successfully.',
    'refresh_token' => 'refresh token was successfully.',
    'is_thumb_not_delete' => ':model is thumb',
    'not_same_enterprising' => ':model not shared with this enterprising user',
    'not_has_enterprising' => ':model can not being retrieved by user without enterprising',
    'not_authorized_app' => 'You have no permission to  APP',
    'not_authorized_list' => 'You have no permission to list :model',
    'not_authorized_create' => 'You have no permission to create :model',
    'not_authorized_read' => 'You have no permission to read :model',
    'not_authorized_update' => 'You have no permission to update :model',
    'not_authorized_delete' => 'You have no permission to delete :model',
    'not_authorized_import' => 'You have no permission to import :model',
    'not_authorized_export' => 'You have no permission to export :model',
    'not_authorized_print' => 'You have no permission to print :model',
    'not_authorized_login' => 'You have no permission to access',
    'http_internal_server_error' => 'Oops, we are having trouble with the server. But our technicians are already working to resolve this. Please try again in a few minutes, please.',
    'request_has_errors' => 'Request has errors',
    'entity_has_dependency' => ':model has dependencies in other tables, could not executed',
    'not_authorized_delete_node' => 'Can not delete a generic node :model',
    'not_allowed_measurements_not_is_leaf' => 'Can no add measurements to not activity level :model',

    'email_send_success' => ':model email sended successfully',
    'app' => [
        'register' => 'Verifique em seu e-mail as instruções para completar o seu registro e termos de uso do aplicativo',
        'forget_password' => 'Verifique em seu e-mail as instruções para recuperação de sua senha',
        'forget_password_changed' => 'Password changed successfully!',

    ]
];
