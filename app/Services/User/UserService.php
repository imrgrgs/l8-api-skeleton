<?php

namespace App\Services\User;

use App\Models\User;
use App\Services\Params\LocaleParamService;

class UserService
{
    public function store(array $input)
    {
        $name = $input['name'];
        $email = $input['email'];
        $password = $input['password'];

        if (empty($input['locale'])) {
            $input['locale'] = LocaleParamService::getDefault();
        }
        $locale = $input['locale'];
        $groupAccess = $input['group_access'];
        $tenantId = $input['tenant_id'];
        $role = $input['role']; // verificar role exists
        $user = User::create([
            'name' => ucwords($name),
            'email' => $email,
            'password' => bcrypt($password),
            'group_access' => $groupAccess,
            'locale' => $locale,
            'active' => true,
        ]);
        $user->attachRole($role);
        $user->tenants()->attach($tenantId);
        return $user;
    }
}
