<?php

namespace App\Services\Tenant;

use App\Models\Tenant;

class TenantService
{
    public function getMaster()
    {
        return Tenant::where('is_master', 1)->firstOrFail();
    }
}
