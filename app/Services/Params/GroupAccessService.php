<?php

namespace App\Services\Params;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class GroupAccessService
{
    static $default = 'frontend';

    public static function getValues(): array
    {
        $params = [];
        $paramName = 'group_access';
        $params = Config::get('app_params.' . $paramName);
        if ($params === null) {
            $error = __('group_access array not found in config\app_params.php');
            Log::critical($error);
        }
        return $params;
    }

    public static function getAllowed(): array
    {
        $allowed = [];
        $params = self::getValues();
        foreach ($params as $key => $value) {
            if (!empty($value['is_active']) && $value['is_active'] == true) {
                $allowed[] = $key;
            }
        }
        return $allowed;
    }

    public static function getDefault(): string
    {
        $default = self::$default;
        $params = self::getValues();
        foreach ($params as $key => $value) {
            if (!empty($value['is_default']) && $value['is_default'] == true) {
                $default = $key;
            }
        }
        return $default;
    }

    /**
     * Verifies if given locale exists
     *
     * @param string $locale locale value
     * @return boolean true if exists | false not exists
     */
    public static function exist(string $param): bool
    {
        $exist = false;
        $params = self::getValues();

        foreach ($params as $key => $value) {
            if (!empty($value['value']) && $value['value'] == $param) {
                $exist = true;
            }
        }

        return $exist;
    }
}
