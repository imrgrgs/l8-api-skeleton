<?php

namespace App\Traits;

use Exception;
use App\Models\User;
use Illuminate\Http\JsonResponse;

trait ApiCheckPermission
{
    protected function hasRole($role): bool
    {
        $user = User::where('id', auth()->user()->id)->firstOrFail();
        $hasRole = true;
        if (!$user->hasRole($role)) {
            $hasRole = false;
        }
        return $hasRole;
    }

    protected function hasPermission($permissions): bool
    {
        $user = User::where('id', auth()->user()->id)->firstOrFail();
        $hasPermissions = true;
        if (!$user->isAbleTo($permissions)) {
            $hasPermissions = false;
        }
        return $hasPermissions;
    }

    protected function isActive(): bool
    {
        $user = User::where('id', auth()->user()->id)->firstOrFail();
        $active = true;
        if (!$user->active) {
            $active = false;
        }
        return $active;
    }

    protected function hasTenant(): bool
    {
        $user = User::where('id', auth()->user()->id)->firstOrFail();
        $hasTenant = true;
        if (!$user->tenant_id) {
            $hasTenant = false;
        }
        $tenant = $user->tenant;
        if (!$tenant) {
            $hasTenant = false;
        }
        return $hasTenant;
    }

    protected function hasGroupAccess($groupAccess): bool
    {
        $user = User::where('id', auth()->user()->id)->firstOrFail();
        $hasGroupAccess = true;
        if ($user->group_access != $groupAccess) {
            $hasGroupAccess = false;
        }

        return $hasGroupAccess;
    }
}
