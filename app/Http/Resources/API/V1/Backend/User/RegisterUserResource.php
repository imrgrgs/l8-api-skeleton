<?php

namespace App\Http\Resources\API\V1\Backend\User;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\API\V1\Tenant\TenantResource;

class RegisterUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [

            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'active' => $this->active,
            'link' => $this->link,
            'last_login' => $this->last_login,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'locale' => $this->locale,
            'group_access' => $this->group_access,

            'tenants' => TenantResource::collection($this->tenants),


        ];
    }
}
