<?php

namespace App\Http\Resources\API\V1\Auth;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\API\V1\Tenant\TenantResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'user' => [
                'id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
                'active' => $this->active,
                'link' => $this->link,
                'last_login' => $this->last_login,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'locale' => $this->locale,
            ],
            'tenants' => TenantResource::collection($this->tenants),
            'access' => [
                'token' => $this->access_token,
                'token_type' => $this->token_type,
                'expires_in_minutes' => $this->expires_in_minutes,
                'expires_in_hours' => $this->expires_in_hours,
                'group_access' => $this->group_access,

            ]

        ];
    }
}
