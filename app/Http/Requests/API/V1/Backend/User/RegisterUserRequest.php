<?php

namespace App\Http\Requests\API\V1\Backend\User;


use App\Http\Rules\UserRules;


use App\Http\Requests\API\V1\ApiFormRequest;



class RegisterUserRequest extends ApiFormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function filters()
    {

        return UserRules::getFilters();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function store()
    {
        $rules = UserRules::getStore_rules();

        return $rules;
    }
}
