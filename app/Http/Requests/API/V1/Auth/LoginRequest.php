<?php

namespace App\Http\Requests\API\V1\Auth;

use App\Traits\ApiResponser;
use App\Http\Rules\LoginRules;
use App\Traits\ApiCheckPermission;

use App\Http\Requests\API\V1\ApiFormRequest;



class LoginRequest extends ApiFormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function filters()
    {

        return LoginRules::getFilters();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function store()
    {
        $rules = LoginRules::getStore_rules();

        return $rules;
    }
}
