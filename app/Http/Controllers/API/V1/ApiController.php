<?php

namespace App\Http\Controllers\API\V1;

use App\Facades\TenantService;
use Exception;
use App\Models\User;

use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use App\Traits\ApiCheckPermission;
use App\Http\Controllers\Controller;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class ApiController
 */
class ApiController extends Controller
{
    use ApiResponser;
    var $user;
    private function setUser()
    {
        $this->user = User::where('id', auth()->user()->id)->firstOrFail();
    }

    protected function isActive(): bool
    {
        if (!$this->user) {
            $this->setUser();
        }
        $user = $this->user; //User::where('id', auth()->user()->id)->firstOrFail();
        $active = true;
        if (!$user->active) {
            $message = __('auth.user_not_active');
            throw new Exception($message, JsonResponse::HTTP_FORBIDDEN);
            //            $this->sendError($message, JsonResponse::HTTP_FORBIDDEN);
        }
        return $active;
    }

    protected function hasRole($role): bool
    {
        if (!$this->user) {
            $this->setUser();
        }
        $user = $this->user;
        $hasRole = true;
        if (!$user->hasRole($role)) {
            $message = __('auth.user_not_role');
            throw new Exception($message, JsonResponse::HTTP_FORBIDDEN);
        }
        return $hasRole;
    }

    protected function hasPermission($permissions): bool
    {
        if (!$this->user) {
            $this->setUser();
        }
        $user = $this->user;
        $hasPermissions = true;
        if (!$user->isAbleTo($permissions)) {
            $message = __('auth.user_not_permission');
            throw new Exception($message, JsonResponse::HTTP_FORBIDDEN);
        }
        return $hasPermissions;
    }



    protected function hasTenant(): bool
    {
        if (!$this->user) {
            $this->setUser();
        }
        $user = $this->user;
        $hasTenant = true;
        $tenants = $user->tenants;
        if (!$tenants) {
            $message = __('auth.user_has_no_tenant');
            throw new Exception($message, JsonResponse::HTTP_FORBIDDEN);
        }
        return $hasTenant;
    }

    protected function hasMasterTenant(): bool
    {
        $this->hasTenant();

        $user = $this->user;
        $hasMasterTenant = false;
        $tenants = $user->tenants;

        $masterTenant = TenantService::getMaster();
        foreach ($tenants as $tenant) {
            if ($tenant->id == $masterTenant->id) {
                $hasMasterTenant = true;
            }
        }
        if (!$hasMasterTenant) {
            $message = __('auth.user_has_no_master_tenant');
            throw new Exception($message, JsonResponse::HTTP_FORBIDDEN);
        }
        return $hasMasterTenant;
    }

    protected function hasGroupAccess($groupAccess): bool
    {
        if (!$this->user) {
            $this->setUser();
        }
        $user = $this->user;
        $hasGroupAccess = true;
        if ($user->group_access != $groupAccess) {
            $message = __('auth.user_has_no_group_access');
            throw new Exception($message, JsonResponse::HTTP_FORBIDDEN);
        }

        return $hasGroupAccess;
    }

    protected function isBackendUser(): bool
    {

        $isBackendUser = true;
        $this->isActive();
        $this->hasGroupAccess('backend');
        $this->hasMasterTenant();
        return $isBackendUser;
    }

    protected function isFrontendUser(): bool
    {

        $isFrontendUser = true;
        $this->isActive();
        $this->hasGroupAccess('frontend');
        $this->hasTenant();
        return $isFrontendUser;
    }
}
