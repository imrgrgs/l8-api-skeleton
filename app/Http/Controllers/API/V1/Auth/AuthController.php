<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Models\User;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\API\V1\ApiController;
use App\Http\Requests\API\V1\Auth\LoginRequest;
use App\Http\Resources\API\V1\Auth\LoginResource;


class AuthController extends ApiController
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->all(['email', 'password']);
        $token = auth()->attempt($credentials);

        if (!$token) {
            return $this->sendError(
                __('auth.failed', ['model' => __('models/users.singular')])
            );
        }

        $this->isActive();

        $user = User::where('id', auth()->user()->id)->firstOrFail();
        $user->last_login = now();
        $user->update();
        return $this->respond($token);
    }

    public function logout()
    {
        auth()->logout();
        return $this->sendResponse([], __('messages.logout'));
    }

    public function refresh()
    {
        // if (!$this->isActive()) {
        //     $message = __('auth.user_not_active');
        //     $this->sendError($message, JsonResponse::HTTP_FORBIDDEN);
        // }
        $this->isActive();
        return $this->respond(auth()->refresh());
    }

    protected function respond($token)
    {
        $tokenType = 'bearer';
        $userAuth = auth()->user();
        $expires_in_minutes = auth()->factory()->getTTL();
        $expires_in_hours = $expires_in_minutes / 60;
        $userAuth->access_token = $token;
        $userAuth->token_type = $tokenType;
        $userAuth->expires_in_minutes = $expires_in_minutes;
        $userAuth->expires_in_hours = $expires_in_hours;
        return $this->sendResponse(new LoginResource($userAuth), __('messages.retrieved', ['model' => __('models/users.singular')]));
    }
}
