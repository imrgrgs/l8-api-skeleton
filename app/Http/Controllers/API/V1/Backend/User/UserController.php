<?php

namespace App\Http\Controllers\API\V1\Backend\User;

use App\Facades\UserService;
use App\Facades\TenantService;
use App\Http\Controllers\API\V1\ApiController;
use App\Http\Requests\API\V1\Backend\User\RegisterUserRequest;
use App\Http\Resources\API\V1\Backend\User\RegisterUserResource;

class UserController extends ApiController
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    public function store(RegisterUserRequest $request)
    {
        $this->isBackendUser();
        $this->hasPermission('users-create');
        $tenantMaster = TenantService::getMaster();
        $input = $request->all();
        $input['group_access'] = 'backend';
        $input['role'] = 'backend_master';
        $input['tenant_id'] = $tenantMaster->id;


        $user = UserService::store($input);
        $data = new RegisterUserResource($user);
        return $this->sendResponse($data);
    }
}
