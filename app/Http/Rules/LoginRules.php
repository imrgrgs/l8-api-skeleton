<?php

namespace App\Http\Rules;

class LoginRules
{
    public static $filters = [
        'email' => 'trim|escape|lowercase',
    ];

    public static $store_rules = [
        'email' => 'required|email',
        'password' => 'required|min:8|max:32',
    ];

    /**
     * Get the value of filters
     */
    public static function getFilters()
    {
        return self::$filters;
    }

    /**
     * Get the value of store_rules
     */
    public static function getStore_rules()
    {
        return self::$store_rules;
    }
}
