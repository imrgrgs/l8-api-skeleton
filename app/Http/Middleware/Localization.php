<?php

namespace App\Http\Middleware;



use Closure;

use App\Traits\ApiResponser;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Services\Params\LocaleParamService;


class Localization
{
    use ApiResponser;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Log::debug("User Localization " . $request->user());
        $defaultLocale = ''; //auth()->user()->locale;
        if (!$defaultLocale) {
            $defaultLocale = LocaleParamService::getDefault();
        }
        // Check header request and determine localizaton
        $locale = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : $defaultLocale;

        if ($locale == app()->getLocale()) {
            // continue request
            return $next($request);
        }
        if ($locale == $defaultLocale) {
            // set laravel localization
            app()->setLocale($locale);
            // continue request
            return $next($request);
        }
        /*|-------------------------------------------------------------------------
          | Leitura de params para obter os locales válidos param-name = 'locales'
          |-------------------------------------------------------------------------
          */


        $exist = LocaleParamService::exist($locale);
        if (!$exist) {
            $allowedLocales = LocaleParamService::getAllowed();

            $error = __('messages.locale_not_allowed', ['attribute' => $request->header('X-localization'), 'allowed' => implode(', ', $allowedLocales)]);

            return $this->sendError($error, JsonResponse::HTTP_PRECONDITION_FAILED);
        }

        // set laravel localization
        app()->setLocale($locale);
        // continue request
        return $next($request);
    }
}
