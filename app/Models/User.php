<?php

namespace App\Models;

use App\Traits\ApiLogsActivity;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

use Tymon\JWTAuth\Contracts\JWTSubject;


use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use LaratrustUserTrait;
    const AVATAR_STORAGE = 'public/images/avatars';
    const AVATAR_THUMB_STORAGE = 'public/images/avatars/thumb';
    const AVATAR_DEFAULT = 'default-avatar.png';

    use ApiLogsActivity;
    use SoftDeletes;
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'tenant_id',
        'name',
        'email',
        'password',
        'group_access',
        'active',
        'avatar',
        'file_avatar',
        'locale',
        'module',
        'provider', // social login
        'provider_id', // social login
        'provider_response' // social login
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'provider', // social login
        'provider_id', // social login
        'provider_response' // social login

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'active' => 'boolean',
        'last_login' => 'datetime',
    ];


    /*|-----------------------------------------------------------------------
      | Appends = calculated attributes
      |-----------------------------------------------------------------------
      */

    protected $appends = ['_link',];

    /**
     * @return User link to show
     **/
    public function getLinkAttribute()
    {
        if ($this->avatar) {
            $href = URL::to('/') . Storage::url(self::AVATAR_STORAGE) . '/' . $this->avatar;
        } else {
            $href = URL::to('/') . Storage::url(self::AVATAR_STORAGE) . '/' . self::AVATAR_DEFAULT;
        }
        return [
            'href_avatar' => $href,

        ];
    }

    /*|-----------------------------------------------------------------------
      | END Appends = calculated attributes
      |-----------------------------------------------------------------------
      */

    /*|----------------------------------------------------------------------------
      | Json Web Token for API access
      | It is part of tymon/jwt-auth package
      |----------------------------------------------------------------------------
      */

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    /*|----------------------------------------------------------------------------
      | END Json Web Token for API access
      |----------------------------------------------------------------------------
      */

    /*|-------------------------------------------------------
      | Relationships
      |-------------------------------------------------------
      */

    /**
     * Undocumented function
     *
     * @return void
     */
    public function tenants()
    {
        return $this->belongsToMany(\App\Models\Tenant::class, 'tenant_users', 'user_id', 'tenant_id');
    }

    /*|-----------------------------------------------------------------------
      | END Relationships
      |-----------------------------------------------------------------------
      */
}
