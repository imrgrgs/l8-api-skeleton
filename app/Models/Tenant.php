<?php

namespace App\Models;

use App\Traits\ApiLogsActivity;
use App\Traits\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tenant extends Model
{
    use CascadeSoftDeletes;
    use HasFactory;
    use SoftDeletes;
    use ApiLogsActivity;

    protected $cascadeDeletes = ['users'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'is_master',
        'is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'code',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_master' => 'boolean',
        'is_active' => 'boolean',
    ];


    /*|-----------------------------------------------------------------------
      | Appends = calculated attributes
      |-----------------------------------------------------------------------
      */

    protected $appends = [
        //
    ];
    /*|-----------------------------------------------------------------------
      | END Appends = calculated attributes
      |-----------------------------------------------------------------------
      */


    /*|-------------------------------------------------------
      | Relationships
      |-------------------------------------------------------*/


    /**
     * Return a collection
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|null
     */
    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'tenant_users', 'tenant_id', 'user_id');
        //        return $this->hasMany(\App\Models\User::class, 'tenant_id', 'id');
    }
    /*|-----------------------------------------------------------------------
      | END Relationships
      |-----------------------------------------------------------------------
      */
}
